" Synchronise Vim with the tmux and X11 paste buffers
" Author: Evan Hanson <evhan@foldling.org>
" Last Change: 2019-06-04
" Version: 0.0.1
" Homepage: https://git.foldling.org/vim-connecticut.git
" Repository: https://git.foldling.org/vim-connecticut.git
" License: BSD

fun! connecticut#ImportFromTmux()
  let l:text = system('tmux show-buffer')
  if @" !=# l:text | let @" = l:text | endif
endfun

fun! connecticut#ImportFromX11()
  let l:text = system(g:connecticut_x11_clipboard_command . ' -o')
  if @" !=# l:text | let @" = l:text | endif
endfun

fun! connecticut#Paste(motion)
  call connecticut#ImportFromX11()
  execute 'normal! ' . a:motion
endfun

fun! connecticut#ExportFromVim(event)
  let l:name = a:event['regname']
  let l:command = g:connecticut_x11_clipboard_command . ' -i'

  if empty(l:name) || l:name ==# '"'
    let l:type = a:event['regtype']
    let l:text = join(a:event['regcontents'], "\n")

    if l:type =~# 'V'
      let l:text = l:text . "\n"
    endif

    if g:connecticut_sync_tmux_buffer && strlen($TMUX)
      let l:command = l:command . '; ' . g:connecticut_x11_clipboard_command . ' -o | tmux load-buffer -'
    endif

    call system(l:command, l:text)
  endif
endfun

fun! connecticut#Enable()
  if exists('##TextYankPost') == 0
    return
  endif

  augroup connecticut
    autocmd!
    autocmd TextYankPost * call connecticut#ExportFromVim(v:event)
  augroup END

  execute 'nnoremap <silent>' . g:connecticut_map_put_after  . ' :call connecticut#Paste("p")<cr>'
  execute 'nnoremap <silent>' . g:connecticut_map_put_before . ' :call connecticut#Paste("P")<cr>'
  execute 'vnoremap <silent>' . g:connecticut_map_put_after  . ' :<c-u>call connecticut#Paste("gvp")<cr>'
  execute 'vnoremap <silent>' . g:connecticut_map_put_before . ' :<c-u>call connecticut#Paste("gvP")<cr>'
endfun

fun! connecticut#Initialize(options)
  for [l:key, l:value] in items(a:options)
    if exists("g:connecticut_" . l:key) == 0
      execute "let g:connecticut_" . l:key . " = " . string(l:value)
    endif
  endfor

  call connecticut#Enable()
endfun
