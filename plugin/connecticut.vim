" Synchronise Vim with the tmux and X11 paste buffers
" Author: Evan Hanson <evhan@foldling.org>
" Last Change: 2019-06-03
" Version: 0.0.1
" Homepage: https://git.foldling.org/vim-connecticut.git
" Repository: https://git.foldling.org/vim-connecticut.git
" License: BSD

if exists('g:connecticut_loaded') || &compatible || version < 800
  finish
endif

call connecticut#Initialize({
\   'sync_tmux_buffer' : 1
\ , 'map_put_after' : 'p'
\ , 'map_put_before' : 'P'
\ , 'x11_clipboard_command' : 'xsel'
\ })

let g:connecticut_loaded = 1
