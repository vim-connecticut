vim-connecticut
===============
Synchronise Vim with the tmux and X11 paste buffers.

Requirements
------------
 - Vim 8.0 or newer
 - [xsel][] (or some other X11 selection manager)

[xsel]: http://www.vergenet.net/~conrad/software/xsel/

Installation
------------
Use your plugin manager of choice to install this repository.

Usage
-----
This plugin tries to ensure that, whenever you paste text, the result is
the same whether you're pasting into Vim, tmux, or X11.

For more information, refer to the plugin's [help file][help].

Note that this plugin can only manage Vim's side of things. You'll need
to use some other software if you want to keep tmux and X11 in sync
independently.

[help]: https://git.foldling.org/vim-connecticut.git/tree/master/doc/connecticut.txt

Licence
------
Copyright (c) 2019 Evan Hanson <evhan@foldling.org>

BSD-style license. See `LICENSE` for details.
